<?php
define("TIMER_DIRECTORY", "timers/");
// define("TIMER_ACTIVE_FILENAME", "timer_in_progress.txt");
// define("TIMER_END_TIME_FILENAME", "timer_end_time.txt");

function filename_for_id($id){
    $id = preg_replace( '/[\W]/', '', $id);
    return TIMER_DIRECTORY . $id . ".txt";
}

function return_state($id){
    // Strip all non-alphanumeric characters from id
    
    $timer_filename = filename_for_id($id);

    if (file_exists($timer_filename)){ 
        $state = 1; 
        $end_time = intval(file_get_contents($timer_filename));        
    }
    else { 
        $state = 0; 
        $end_time = 0;
    }

    $return_json = json_encode(array(
        "id" => $id,
        "state" => $state,
        "end_time" => $end_time
    ));
    
    echo $return_json;
    exit();

}


if (isset($_POST)){
    //print_r($_POST);

    if (isset($_POST['command'])){
        // Strip non-alphanumeric characters
        if (isset($_POST['id'])){ $id = preg_replace( '/[\W]/', '', $_POST['id']);}
        else { 
            echo json_encode(array("error" => "no id set"));
            exit();
        }

        if ($_POST['command'] == "start"){
            if (isset($_POST['minutes'])){ $minutes = intval($_POST['minutes']); }
            else { $minutes = 0; }
            if (isset($_POST['seconds'])){ $seconds = intval($_POST['seconds']); }
            else { $seconds = 0; }

            $timer_end = time() + $seconds + $minutes * 60;
            
            // Write timer end to file
            $timer_filename = filename_for_id($id);
            $file = fopen($timer_filename, 'w');
            fwrite($file, strval($timer_end));
            fclose($file);

            return_state($id);
        }
        else if ($_POST['command'] == "stop"){
            $timer_filename = filename_for_id($id);
            if (is_file($timer_filename)){ unlink($timer_filename); }
            return_state($id);
        }
        else if ($_POST['command'] == "get_state"){
            return_state($id);
        }
        
    }
    
}

?>
