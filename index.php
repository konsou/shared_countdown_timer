<?php
if (isset($_GET['id'])){ $id = $_GET['id']; }
else { $id = uniqid(); }
?>
<!DOCTYPE html>
<html>
	<head>
		<script src="jquery-3.4.1.min.js"></script>
		<script src="scripts_ajax.js"></script>
        <title>Shared Countdown Timer</title>
        <link rel="stylesheet" href="styles.css">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163209301-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-163209301-1');
        </script>
	</head>
	<body>
        <div id="container">
            <div id="timer-block">
                <form id="ftime">
                    <input id="fminutes" name="fminutes" type="text" class="time-input" style="text-align:right;" size="2" value="30" />
                    <label for="fminutes">minuuttia</label>
                    <br>
                    <input id="fseconds" name="fseconds" type="text" class="time-input" style="text-align:right;" size="2" value="0" />
                    <label for="fseconds">sekuntia</label>
                    <input type="hidden" name="fid" id="fid" value="<?php echo $id; ?>">
                </form>
                <div id="buttons">
                    <button type="button" id="start">Aloita</button>
                    <button type="button" id="stop">Lopeta</button>
                </div>
                <div id="share">
                    <a href="index.php?id=<?php echo $id; ?>">Linkki tähän ajastimeen (id <?php echo $id; ?>)</a>
                </div>
                <audio id="alarm_sound">
                    <source src="alarm_2_secs.mp3" type="audio/mpeg">
                </audio>
            </div>
        </div>
        <div id="footer">Lähdekoodi saatavilla: <a href="https://bitbucket.org/konsou/shared_countdown_timer/src/master/" target="_blank">Bitbucket</a></div>
	</body>
</html>