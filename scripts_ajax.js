function startTimer(){
    //console.log("Start button clicked")
    var minutes = parseInt($("#fminutes").val());
    var seconds = parseInt($("#fseconds").val());
    var id = $("#fid").val();

    request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: {
                "command": "start",
                "id": id,
                "minutes": minutes,
                "seconds": seconds
            }
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
        response_parsed = JSON.parse(response);
        updateUI(response_parsed.state, response_parsed.end_time);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
}

function stopTimer(){
    //console.log("Stop button clicked")
    var id = $("#fid").val();

    request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: {
                "command": "stop",
                "id": id,
            }
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
        response_parsed = JSON.parse(response);
        updateUI(response_parsed.state, response_parsed.end_time);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });
}

function getTimerState(){
    var state, end_time;
    var id = $("#fid").val();

    request = $.ajax({
        url: "ajax_calls.php",
        type: "post",
        data: {
                "command": "get_state",
                "id": id,
            }
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        console.log(response);
        response_parsed = JSON.parse(response);

        var now = Math.floor(Date.now() / 1000);

        if (response_parsed.end_time != 0 && 
          response_parsed.end_time < now && 
          localStorage.getItem("last_state") == 1){
            // Play alarm if countdown finished
            $("#alarm_sound")[0].play();
            response_parsed.state = 0;
        }

        // Stop countdown if time has passed
        if (response_parsed.end_time < now && response_parsed.state == 1){
            stopTimer();
        }

        localStorage.setItem("last_state", response_parsed.state);
        
        updateUI(response_parsed.state, response_parsed.end_time);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
        updateUI(NaN, NaN);
    });

}

function updateUI(state, end_time){
    //console.log("In updateUI - state: "+ state + ", end_time: "+ end_time);
    var now = Math.floor(Date.now() / 1000);
    var time_left = end_time - now;
    var minutes = Math.floor(time_left / 60);
    var seconds = time_left % 60;

    minutes = Math.max(minutes, 0);
    seconds = Math.max(seconds, 0);


    if (state == 1){ 
        $("#fminutes").val(minutes);
        $("#fseconds").val(seconds);
    
        $("#start").prop("disabled", true); 
        $("#stop").prop("disabled", false); 
    }
    // state == 0
    else { 
        $("#start").prop("disabled", false); 
        $("#stop").prop("disabled", true); 
    }
}


$( document ).ready(function() {
    getTimerState();

    // Variable to hold request
    var request;

    // Enter key press in input fields starts timer
    $('.time-input').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            if (!$("#start").prop("disabled")){ startTimer(); }
        }
    });

    $("#start").click(function(){
        startTimer();
    });

    $("#stop").click(function(){
        stopTimer();
    });

    $("#check").click(function(){
        getTimerState();
    });

    window.setInterval(function(){
		getTimerState();
	}, 1000);

});